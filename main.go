package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"text/template"

	_ "github.com/lib/pq"
	"github.com/spf13/viper"
	"golang.org/x/crypto/bcrypt"
)

var db *sql.DB
var tpl *template.Template

type Configuration struct {
	Server   ServerConfiguration
	Database DatabaseConfiguration
	Matrix   MatrixConfiguration
}

type DatabaseConfiguration struct {
	Host       string
	Port       int
	DBname     string
	DBuser     string
	DBpassword string
}

type ServerConfiguration struct {
	Host   string
	Port   int
	Secret string
}

type MatrixConfiguration struct {
	Host string
	Port int
}

func init() {

	tpl = template.Must(template.ParseGlob("templates/*.gohtml"))

}

var configuration Configuration

func main() {

	viper.SetConfigName("config")
	viper.AddConfigPath("$HOME/.config/goMatrixregister/")
	viper.AddConfigPath(".")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}

	err := viper.Unmarshal(&configuration)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}
	//Debug
	log.Printf("database host is %s", configuration.Database.Host)
	log.Printf("I'm running on host  %s", configuration.Server.Host)

	// set URI for server

	listenServer := fmt.Sprintf("%s:%d", configuration.Server.Host, configuration.Server.Port)

	initDB()
	// Create public folder to add static pages like CSS and images etc
	fs := http.FileServer(http.Dir("./public"))
	http.Handle("/meow/public/", http.StripPrefix("/meow/public/", fs))
	//http.Handle("/meow/public/", fs)

	// we change root path and everything after  depending on where our entry point is.
	//http.HandleFunc("/", index)
	http.HandleFunc("/meow", index)
	http.HandleFunc("/meow/manage", manage)
	http.HandleFunc("/meow/signin", signin)
	http.HandleFunc("/meow/signup", signup)
	http.HandleFunc("/meow/register", registration)
	//http.Handle("/meow/public/", http.StripPrefix("/public/", fs))

	http.ListenAndServe(listenServer, nil)

}

func index(w http.ResponseWriter, r *http.Request) {

	tpl.ExecuteTemplate(w, "index.gohtml", nil)

}

func manage(w http.ResponseWriter, r *http.Request) {

	tpl.ExecuteTemplate(w, "signup.gohtml", nil)

}

func initDB() {
	var err error
	// Connect to the postgres db

	fmt.Println("Debug: Connecting to DB")
	//you might have to change the connection string to add your database credentials
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=require",
		configuration.Database.Host,
		configuration.Database.Port,
		configuration.Database.DBuser,
		configuration.Database.DBpassword,
		configuration.Database.DBname)

	db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		fmt.Printf("There was a error connecting to postgres: %s\n", err)
		panic(err)
	}
	fmt.Println("DEBUG: we go to end of db func")
}

func signin(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {

		http.Redirect(w, r, "/meow", http.StatusSeeOther)
		return
	}

	username := r.FormValue("user")
	password := r.FormValue("password")

	var databaseUsername string
	var databasePassword string
	fmt.Println("DEBUG: about to do the QueryRow")

	// Get the existing entry present in the database for the given username
	sqlStatement := `SELECT username, password FROM users WHERE username=$1`
	fmt.Println("DEBUG: variable of sqlStatement done")
	row := db.QueryRow(sqlStatement, username)
	err := row.Scan(&databaseUsername, &databasePassword)

	if err != nil {
		if err == sql.ErrNoRows {
			fmt.Println("Zero rows found")
			http.Redirect(w, r, "/meow", 301)
			return
		} else {
			fmt.Printf("some thing happened %s", err)
			http.Redirect(w, r, "/meow", 301)
		}
	}

	// Compare the stored hashed password, with the hashed version of the password that was received
	err = bcrypt.CompareHashAndPassword([]byte(databasePassword), []byte(password))

	if err != nil {
		// If the two passwords don't match, return a 401 status
		http.Redirect(w, r, "/meow", 401)
		return
	}

	// If we reach this point, that means the users password was correct, and that they are authorized
	// sent to registration page
	tpl.ExecuteTemplate(w, "register.gohtml", nil)

}

func signup(w http.ResponseWriter, r *http.Request) {

	if r.Method != "POST" {

		http.Redirect(w, r, "/meow", http.StatusSeeOther)
		return
	}
	username := r.FormValue("user")
	password := r.FormValue("password")

	var user string
	sqlStatement := `SELECT username FROM users WHERE username=$1`
	err := db.QueryRow(sqlStatement, username).Scan(&user)

	switch {
	case err == sql.ErrNoRows:
		// Salt and hash the password using the bcrypt algorithm
		// The second argument is the cost of hashing, im leaving it default for now
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
		if err != nil {
			http.Error(w, "NoRows, hashedPaswiord: Server error, unable to create your account.", 500)
			return
		}
		// Next, insert the username, along with the hashed password into the database
		_, err = db.Exec("insert into users values ($1, $2)", username, string(hashedPassword))
		if err != nil {
			// If there is any issue with inserting into the database, return a 500 error
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		w.Write([]byte("User created!"))
		return
	case err != nil:
		http.Error(w, "Server error, unable to create your account.", 500)
		return
	default:
		http.Redirect(w, r, "/meow", 301)
	}

}

func registration(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {

		http.Redirect(w, r, "/meow", http.StatusSeeOther)
		return
	}
	login := r.FormValue("user")
	password := r.FormValue("password")
	var body string

	d := struct {
		Login    string
		Password string
		Body     string
	}{

		Login:    login,
		Password: password,
		Body:     body,
	}
	d.Body = register(login, password)
	tpl.ExecuteTemplate(w, "registration.gohtml", d)

}
