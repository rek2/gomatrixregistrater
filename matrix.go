package main

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type Body struct {
	Nonce string `json:"nonce"`
}

type Data struct {
	Nonce    string `json:"nonce"`
	Username string `json:"username"`
	Password string `json:"password"`
	Admin    bool   `json:"admin"`
	Mac      string `json:"mac"`
}

func register(username, password string) string {
	// we do http because is localhost, we should change this and SPECILLY change this if you are not using localhost
	url := fmt.Sprintf("http://%s:%d", configuration.Matrix.Host, configuration.Matrix.Port)

	fmt.Println(url)
	registerUrl := fmt.Sprintf("%s/_synapse/admin/v1/register", url)
	fmt.Println(registerUrl)
	resp, err := http.Get(registerUrl)
	if err != nil {
		log.Fatal("failed to create mautrix client")
	}
	body, _ := ioutil.ReadAll(resp.Body)

	var f Body

	err = json.Unmarshal(body, &f)
	fmt.Printf("%s\n", f.Nonce)

	mac := hmac.New(sha1.New, []byte(configuration.Server.Secret))
	mac.Write([]byte(f.Nonce))
	mac.Write([]byte{0})
	mac.Write([]byte(username))
	mac.Write([]byte{0})
	mac.Write([]byte(password))
	mac.Write([]byte{0})
	mac.Write([]byte("notadmin"))

	checksum := hex.EncodeToString(mac.Sum(nil))

	jsonData := Data{
		f.Nonce,
		username,
		password,
		false,
		checksum,
	}

	postData, err := json.Marshal(jsonData)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("%+v\n", bytes.NewBuffer(postData))
	resp, err = http.Post(registerUrl, "application/json", bytes.NewBuffer(postData))
	if err != nil {
		log.Fatal("failed to create post to register new matrix user")
	}
	defer resp.Body.Close()
	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)
	response := buf.String()
	fmt.Println(response)
	return response
}
